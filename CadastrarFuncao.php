<?php
// definições de host, database, usuário e senha
$conexao= mysqli_connect('localhost', 'root', '' , 'siscapes');
mysqli_set_charset($conexao, 'utf8');
    
$db = "siscapes";
mysqli_select_db($conexao, $db);
// cria a instrução SQL que vai selecionar os dados
$query = sprintf("SELECT funcao.CodFuncao, funcao.descricao FROM funcao");
//$query = sprintf("SELECT usuario.nome, funcao.nome FROM usuario, funcao WHERE usuario.funcao = funcao.CodFuncao");
// executa a query
$dados = mysqli_query($conexao,$query);

// transforma os dados em um array
//$linha = mysqli_fetch_assoc($dados);


// calcula quantos dados retornaram
//$total = mysqli_num_rows($dados);

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Painel Administrativo 2 Cadastrar Funcao</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Cadastro de Funções</div>
      <div class="card-body">
        <form method="POST">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
               
              </div>
              <div class="col-md-12">
                <label for="exampleInputLastName">Nome da Função</label>
                <input class="form-control" name="nomefuncao" id="exampleInputLastName" type="text" aria-describedby="nameHelp" placeholder="Digite o nome da Função">
              </div>
            </div>
          </div>
         
          
          <input type="submit" class="btn btn-primary btn-block" href="#" name="enviar" data-toggle="modal" data-target="#delete-modal" value="Cadastrar">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="PainelFuncoes.php">Sair</a>
          
        </div>
      </div>
    </div>
  </div>
                   <!-- Modal -->
<!--<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLabel">Cadastro de Funções</h4>
                </div>
                <div class="modal-body">Função Cadastrada com Sucesso! </div>
                <div class="modal-footer">
                    
                   
                </div>
            </div>
        </div>
    </div> -->
       </div>
       <?php

       if (isset($_POST['enviar'])) 
       {
         $namefuncao = $_POST['nomefuncao'];

          $sql = "INSERT INTO funcao values";
          $sql .="('', '$namefuncao')";
        
        if ($conexao->query($sql) === true){
            
        ?>

            <script type="text/javascript">
                alert("Cadastrado!");
                setTimeout ("window.location='PainelFuncoes.php'");
            </script>
           
        <?php
            
          }else {
            echo "Erro: " . $sql . "<br>" . $conexao->error;
          }

          $conexao->close();


       }

      
       ?>
       
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>