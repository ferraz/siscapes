<?php
include 'conecta_mysqli.php';
$db = "siscapes";
mysqli_select_db($conexao, $db);

$codigo = $_GET['CodFuncao'];

// cria a instrução SQL que vai selecionar os dados
$query = sprintf("SELECT funcao.CodFuncao, funcao.Descricao FROM funcao WHERE funcao.CodFuncao = '$codigo'");
//$query = sprintf("SELECT usuario.nome, funcao.nome FROM usuario, funcao WHERE usuario.funcao = funcao.CodFuncao");
// executa a query
$dados = mysqli_query($conexao,$query);

// transforma os dados em um array
$linha = mysqli_fetch_assoc($dados);

echo $codigo;

// calcula quantos dados retornaram
//$total = mysqli_num_rows($dados);

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Painel Administrativo 2 Cadastrar Usuário</title>

   <script type="text/javascript">
    function attsucesso(){
      alert("Funcao editada com sucesso");
      setTimeout ("window.location='PainelFuncoes.php'");
    }
  </script>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Editar Função</div>
      <div class="card-body">
        <form method="POST">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                
              </div>
              <div class="col-md-12">
                <label for="exampleInputLastName">Nome da Função</label>
                <input class="form-control" id="exampleInputLastName" name="funcao" type="text" aria-describedby="nameHelp" placeholder="Digite o nome da função" value="<?php echo $linha['Descricao']; ?>">
              </div>
            </div>
          </div>
          
          </div>
          <input type="submit" class="btn btn-primary btn-block" href="" name="salvar" value="Editar">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="PainelFuncoes.php">Sair</a>
         
        </div>
      </div>
    </div>
  </div>
    <?php
      if (isset($_POST['salvar'])) {
          $descricao = $_POST['funcao'];
          echo $codigo;


            $query1 = sprintf("UPDATE funcao SET funcao.Descricao = '$descricao' WHERE funcao.CodFuncao = $codigo");
            echo  "<script>attsucesso();</script>";
            $dados1 = mysqli_query($conexao,$query1);
          //header("location: PainelUsuariosDoSistema.php");
          }

        
        
        ?>
<!--<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLabel">Função Editada</h4>
                </div>
                <div class="modal-body">Função Editada com Sucesso! </div>
                <div class="modal-footer">
                    
                   
                </div>
            </div>
        </div>
    </div> 
       </div>
   Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>