<?php
include 'conecta_mysqli.php';
$db = "siscapes";
mysqli_select_db($conexao, $db);


// cria a instrução SQL que vai selecionar os dados
$CodUsuario = $_GET['CodUsuario'];
$query = sprintf("SELECT usuario.CodUsuario, usuario.nome, usuario.funcao, usuario.email, funcao.codfuncao, funcao.descricao FROM usuario INNER JOIN funcao ON usuario.funcao = funcao.codfuncao WHERE usuario.CodUsuario = $CodUsuario");
// executa a query/
$dados = mysqli_query($conexao,$query);

// transforma os dados em um array
$linha = mysqli_fetch_assoc($dados);
$funcaoAtual = $linha['funcao'];

// calcula quantos dados retornaram
$total = mysqli_num_rows($dados);

$query2 = sprintf("SELECT funcao.codfuncao, funcao.descricao FROM funcao WHERE NOT (codfuncao = $funcaoAtual)");

$d2 = mysqli_query($conexao,$query2);

// transforma os dados em um array
$l2 = mysqli_fetch_assoc($d2);

// calcula quantos dados retornaram
$t2 = mysqli_num_rows($d2);

$result = mysqli_affected_rows($conexao);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Painel Administrativo 2 Cadastrar Usuário</title>

  <script type="text/javascript">
      function editado(){
      if (alert ("Editado com Sucesso!!")){
         SetTimeOut("window.location = 'PainelUsuariosdoSistema.php'",1000); 
          
      }
    }
 </script> 

  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Editar Usuário</div>
      <div class="card-body">
        <form method="POST">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Nome</label>
                <input class="form-control" name="nome" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Digite o Nome" required="Campo Obrigatório" value="<?php echo $linha['nome']; ?>">
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input class="form-control" name="email" id="exampleInputEmail1"  type="email" aria-describedby="emailHelp" placeholder="Digite o Email" value="<?php echo $linha['email']; ?>">
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword">Senha</label>
                <input class="form-control" name="senha" id="exampleInputPassword" type="password" placeholder="Senha" value="">
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword1">Confirmar Senha</label>
                <input class="form-control" name="confSenha" id="exampleConfirmPassword1" type="password" placeholder="Confirmar Senha">
              </div>
              <div class="col-md-6">
                <label for="exampleInputLastFuncao">Funcao</label>
                 <!-- <input class="form-control" id="exampleInputLastFuncao" type="text" aria-describedby="nameHelp" placeholder="Digite o Nome da Funcao" value="<?php echo $linha['descricao']; ?>">> -->

                <select name="funcao" class="form-control" id="exampleSelect" required="" >       
                <option selected="selected" value="<?php echo $linha['codfuncao']; ?>"><?php echo $linha['descricao'];?></option> 
                  <?php
                      // se o número de resultados for maior que zero, mostra os dados
                       if($t2 > 0) {
                               // inicia o loop que vai mostrar todos os dados
                        do {
                  ?>
                  <option value="<?php echo $l2['codfuncao']; ?>"><?php echo $l2['descricao'];?></option>                                
                  <?php
                      // finaliza o loop que vai mostrar os dados
                      }while($l2 = mysqli_fetch_assoc($d2));
                       // fim do if 
                      }                           
                  ?>
                                                
                 <?php
                                               
                    // tira o resultado da busca da memória
                    mysqli_free_result($d2);
                 ?>
                 </select>

              </div>
            </div>
          </div>

          <input type="submit" class="btn btn-primary btn-block" href="EditarUsuario.php" name="salvar"  value="Editar" onClick="javascript:editado();"> 

        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="PainelUsuariosDoSistema.php">Sair</a>
         
        </div>
      </div>
    </div>
  </div>

    <?php

          if (isset($_POST['salvar'])) {
          $nome = $_POST['nome'];
          $email= $_POST['email'];
          $senha = $_POST['senha'];
          $confSenha= $_POST['confSenha'];
          $funcao=$_POST['funcao'];

     if ($senha != $confSenha) {
           echo  "<script>alert('Senhas não correspondem!');</script>";


       }else
       {
         // echo "<script>alert('Usuário editado');</script>";
          echo "<script>window.location = 'PainelUsuariosDoSistema.php';</script>";
       }

         if(($senha !='')&&($senha == $confSenha)){ 
            $query1 = sprintf("UPDATE usuario SET usuario.nome = '$nome', usuario.email = '$email', usuario.senha = '$senha', usuario.funcao = '$funcao' WHERE usuario.CodUsuario = $CodUsuario");
          }else{
            $query1 = sprintf("UPDATE usuario SET usuario.nome = '$nome', usuario.email = '$email', usuario.funcao = '$funcao' WHERE usuario.CodUsuario = $CodUsuario");
          }
        
          $dados1 = mysqli_query($conexao,$query1);
         //header("location: PainelUsuariosDoSistema.php");
          }

        
        
        ?>
                    <!-- Modal 
<div class="modal fade" id="delete-modal"  role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalLabel">Usuário Editado</h4>
                </div>
                  <div class="modal-body">Usuário Editado com Sucesso!
                 </div>
                
            </div>
        </div>
    </div> -->
       
      <script type="text/javascript">
            $(document).ready(function () {
                $('delete-modal').modal('show');
              });
            </script>
   
        <script type="text/javascript">
              $('#myModal').on('shown.bs.modal', function() {
              $('#myInput').focus()
            });
        </script>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>