<?php
  $host = "localhost";
  $user = "root";
  $senha = "";
  $banco = "siscapes";

  $conexao = mysqli_connect($host, $user,$senha) or die (mysqli_error());
  mysqli_select_db($conexao,$banco) or die (mysqli_error());
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Login - Siscapes</title>


  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

  <script type="text/javascript">
    
    function loginsucesso(){
      setTimeout ("window.location='index.html'");
    }

    function loginfalho(){
      setTimeout ("window.location='login.php'",10000000);
    }

  </script>
</head>

<body class="bg-dark">
  <div class="container">

    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">

        <form method="POST" ">
          <div class="form-group">
            <label for="exampleInputEmail1">
              <i class="fa fa-user-circle" aria-hidden="true"></i>
            Usuário</label>
            <input class="form-control" name="login" id="exampleText1" type="text" aria-describedby="textHelp" placeholder="Usuário">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">
              <i class="fa fa-lock" aria-hidden="true"></i>
            Senha</label>
            <input class="form-control" name="senha" id="exampleInputPassword1" type="password" placeholder="Senha">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Lembrar-me a senha</label>
            </div>
          </div>
          <input type="submit" class="btn btn-primary btn-block" name="Entrar" href="" value="Login">
        </form>
        <div class="text-center">
          
          <a class="d-block small" href="RecuperarSenha.php">Esqueci minha Senha?</a>
        </div>
      </div>
    </div>
  </div>

  <?php
  if (isset($_POST['Entrar'])){
    $email = $_POST['login'];
    $senha = $_POST['senha'];

    $sql = mysqli_query($conexao,"SELECT usuario.email, usuario.senha FROM usuario WHERE usuario.email = '$email' and usuario.senha = '$senha'") ;
    $row = mysqli_num_rows($sql);

    if ($row > 0){
      session_start();
        $_SESSION['email'] = $email;
        $_SESSION['senha'] = $senha;

      echo "<script>loginsucesso()</script>";

    }else {
      echo "<center><font color='FFFFF'>Login ou senha incorretos</font></center>";
      echo "<script>loginfalho()</script>";
  }
  }

?>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/popper/popper.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
